// ==UserScript==
// @name         APIGEE Dev Mode
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Sends POST request to localhost:9292
// @author       Jed
// @match        https://apigee.com/platform/*/proxies/**
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    function getLanguage(modeStr) {
        switch (modeStr) {
            case 'ace/mode/xml':
                return '.xml';
            case 'ace/mode/javascript':
                return '.js';
            default:
                return '.txt';
        }
    }

    function keyDownTextField(e) {
        const keyCode = e.keyCode;
        const pressCtrl = e.ctrlKey;

        if (pressCtrl && keyCode==13) {
            const node = document.getElementsByClassName('ace_editor')[0];
            const editor = ace.edit(node);
            const content = editor.getValue();
            const language = getLanguage(editor.session.$modeId);

            const http = new XMLHttpRequest();
            http.open("POST", 'http://localhost:9292');
            http.setRequestHeader('X-Language-Ext', language);
            http.send(content);

            http.onreadystatechange = (e) => {
                if (http.readyState == 4 && http.status == 200) {
                    editor.setValue(http.responseText);
                }
            }
        }
    }

    window.addEventListener(
        "keydown",
        keyDownTextField,
        false
    );
})();
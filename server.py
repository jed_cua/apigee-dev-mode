#!/usr/bin/env python2

import cgi, urlparse
import subprocess
import tempfile, time
import os, sys, re
import stat
import optparse
from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer

_default_port = 9292
_default_editor = "xterm -e nvim"

temp_has_delete=True
processes = {}

class Handler(BaseHTTPRequestHandler):
	global temp_has_delete

	def do_GET(self):
		if self.path == '/status':
		  self.send_response(200)
		  self.send_header('Content-Type', 'text/plain; charset=utf-8')
		  self.end_headers()
		  self.wfile.write('edit-server is running.\n')
		  return
		self.send_error(404, "GET Not Found: %s" % self.path)

	def do_OPTIONS(self):
			self.send_response(200)
			self.send_header('Access-Control-Allow-Origin', '*')
			self.send_header('Access-Control-Allow-Headers', '*')
			self.end_headers()
			return

	def do_POST(self):
		global processes
		try:
			(_, _) = cgi.parse_header(self.headers.getheader('content-type'))

			clength = 0
			cl = self.headers.getheader('content-length')

			if cl != None:
				clength = int(cl)
			else:
				self.send_response(411)
				self.end_headers()
				return

			body = self.rfile.read(clength)
			languageExt = self.headers.getheader('X-Language-Ext')

			print '==============BODY=================='
			if (languageExt == '.xml'):
				body = addXsd(body)

			print body
			print '==============LANGUAGE==============='
			print languageExt
			print '===================================='

			l = [s for s in self.path.split('/') if s]
			print l

			existing_file = self.headers.getheader('x-file')

			# write text into file
			if not existing_file or existing_file == "undefined":
				existing = False
				url = self.headers.getheader('x-url')
				print "url:", url
				prefix = "chrome_"
				if url:
					prefix += re.sub("[^.\w]", "_", re.sub("^.*?//","",url))
				prefix += "_"
				if temp_has_delete==True:
					f = tempfile.NamedTemporaryFile(
							delete=False, prefix=prefix, suffix=languageExt)
					fname = f.name
				else:
					tf = tempfile.mkstemp(prefix=prefix, suffix=languageExt)
					f = os.fdopen(tf[0],"w")
					fname = tf[1]
				print "Opening new file ", fname
			else:
				existing = True
				p = processes[existing_file]
				print "Opening existing file ", existing_file
				f = open(existing_file, "w")
				fname = existing_file

			f.write(body)
			f.close()

			if not existing:
				# spawn editor...
				print "Spawning editor... ", fname
				cmds = ["code", "-n", "-w", fname]
				print cmds

				p = subprocess.call(cmds, close_fds=True)
				processes[fname] = p

			self.send_response(200)

			f = file(fname, 'r')
			s = f.read()
			f.close()

			print '==========RESPONSE BODY============='
			if (languageExt == '.xml'):
				s = removeXsd(s)

			print s
			print '===================================='

			self.send_header('x-open', "true")
			self.send_header('x-file', fname)
			self.send_header('Access-Control-Allow-Origin', '*')
			self.end_headers()
			self.wfile.write(s)
		except :
			print "Error: ", sys.exc_info()[0]
			self.send_error(404, "Not Found: %s" % self.path)

def parse_options():
	parser = optparse.OptionParser()
	parser.add_option(
		"-p", "--port", type="int", dest="port", default=_default_port,
		help="port number to listen on (default: " + str(_default_port) + ")")
	parser.add_option(
		"-e", "--editor", dest="editor", default=_default_editor,
		help='text editor to spawn (default: "' + _default_editor + '")')
	return parser.parse_args()[0]


xsdMap = {
	'AssignMessage' : 'https://raw.githubusercontent.com/apigee/api-platform-samples/master/schemas/policy/assign_message.xsd',
	'BasicAuthentication' : 'https://raw.githubusercontent.com/apigee/api-platform-samples/master/schemas/policy/basic_authentication.xsd',
	'ExtractVariables' : 'https://raw.githubusercontent.com/apigee/api-platform-samples/master/schemas/policy/extract_variables.xsd',
	'FlowCallout' : 'https://raw.githubusercontent.com/apigee/api-platform-samples/master/schemas/policy/flow_callout.xsd',
	'JSONToXML' : 'https://raw.githubusercontent.com/apigee/api-platform-samples/master/schemas/policy/json_to_xml.xsd',
	'Javascript' : 'https://raw.githubusercontent.com/apigee/api-platform-samples/master/schemas/policy/java_script.xsd',
	'KeyValueMapOperations' : 'https://raw.githubusercontent.com/apigee/api-platform-samples/master/schemas/policy/keyvaluemap-operations.xsd',
	'ProxyEndpoint' : 'https://raw.githubusercontent.com/apigee/api-platform-samples/master/schemas/configuration/configuration_schemas.xsd',
	'Quota' : 'https://raw.githubusercontent.com/apigee/api-platform-samples/master/schemas/policy/quota_spike_ratelimit.xsd',
	'ServiceCallout' : 'https://raw.githubusercontent.com/apigee/api-platform-samples/master/schemas/policy/service_callout.xsd',
	'SpikeArrest' : 'https://raw.githubusercontent.com/apigee/api-platform-samples/master/schemas/policy/quota_spike_ratelimit.xsd',
	'TargetEndpoint' : 'https://raw.githubusercontent.com/apigee/api-platform-samples/master/schemas/configuration/configuration_schemas.xsd',
	'XMLToJSON' : 'https://raw.githubusercontent.com/apigee/api-platform-samples/master/schemas/policy/xml_to_json.xsd',
	'OAuthV2' : 'https://raw.githubusercontent.com/apigee/api-platform-samples/master/schemas/policy/oauth_v2_verify_api_key.xsd',
}


def createXsdAttr(tag):
	urlLocation = xsdMap.get(tag)
	return '<{} xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="{}"'.format(tag, urlLocation)


def addXsd(xmlContent):
	temp = xmlContent
	for key in xsdMap.keys():
		temp = temp.replace('<' + key, createXsdAttr(key))
	return temp


def removeXsd(xmlContent):
	temp = xmlContent
	for key in xsdMap.keys():
		temp = temp.replace(createXsdAttr(key), '<' + key)
	return temp


def main():
	global temp_has_delete
	import platform
	t = platform.python_version_tuple()
	if int(t[0]) == 2 and int(t[1]) < 6:
		temp_has_delete = False
		print "Handling lack of delete for NamedTemporaryFile:", temp_has_delete
	options = parse_options()
	Handler.editor = options.editor
	try:
		httpserv = HTTPServer(('localhost', options.port), Handler)
		httpserv.table = {}
		httpserv.serve_forever()
	except KeyboardInterrupt:
		httpserv.socket.close()

if __name__ == '__main__':
	main()

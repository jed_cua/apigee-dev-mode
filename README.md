# APIGEE Dev Mode

### Installation
* Install `client.js` with Tampermonkey for Chrome (or similar extension)
* Run `server.py` with Python2

### Running
* Open a proxy and go to develop tab on Apigee Edge
* Select a proxy/endpoint to edit
* Press `CTRL` + `ENTER` to spawn a code editor
* The code editor configured on `server.py` is Vscode by default, you can change them to your favorite editor